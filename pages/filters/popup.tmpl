template = testcase
title = Popup
description = Check that a filter using the $popup filter option is working as expected.

<section class="testcase-panel">
  <h2>Link based popup</h2>
  <p>Test that a filter using the $popup filter option works on a link based popup.</p>
  <div class="testcase-area">
    <a href="/testfiles/popup/link.html" target="_blank" class="testcase-trigger">Trigger link based popup</a>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>Click the link above.</li>
    <li>The popup opened should be closed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>||{{ site_url|strip_proto }}/testfiles/popup/link.html^$popup</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  <h2 id="script-popup">Script based popup</h2>
  <p>Test that a filter using the $popup filter option works on a script based popup.</p>
  <div class="testcase-area">
    <a href="#script-popup" class="testcase-trigger" onclick="scriptPopup();">Trigger script based popup</a>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>Click the link above.</li>
    <li>The popup opened should be closed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>||{{ site_url|strip_proto }}/testfiles/popup/script.html^$popup</pre></li>
  </ul>
</section>

<script>
  "use strict";

  function scriptPopup()
  {
    window.open("/testfiles/popup/script.html");
  }
</script>
