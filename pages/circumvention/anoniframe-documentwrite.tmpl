template = testcase
title = Anonymous frame document.write()
description = Check that anonymous fame document.write() functionality is working as expected.

<section class="testcase-panel">
  <h2>Frames - Anonymous document.write()</h2>
  <p>Top frame</p>
  <div class="testcase-area">
    <span class="testcase-circ-anoniframe-docwrite" data-expectedresult="fail">This element should be blocked</span>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <p>Sub frame (element.appendChild)</p>
  <div class="testcase-area">
    <iframe id="append"></iframe>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <p>Sub frame (document.write)</p>
  <div class="testcase-area">
    <iframe id="write"></iframe>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The top frame element should be blocked, and the text in both sub frames should be hidden.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>{{ site_url|domain }}##.testcase-circ-anoniframe-docwrite</pre></li>
  </ul>
</section>

<style>
iframe {
  overflow: hidden;
  width: 200px;
  height: 200px;
}

.expected-view iframe {
  width: 98px;
  height: 98px;
  padding: 98px;
}
</style>

<script>
  "use strict";

  let iframe = document.getElementById("append");
  iframe.addEventListener("load", () =>
  {
    let span = document.createElement("span");
    span.className = "testcase-circ-anoniframe-docwrite";
    span.setAttribute("data-expectedresult", "fail");
    span.textContent = "This text should be hidden";
    iframe.contentDocument.body.appendChild(span);
  });

  let doc = document.getElementById("write").contentDocument;
  doc.open();
  doc.write("<span class='testcase-circ-anoniframe-docwrite' " +
            "data-expectedresult='blocked'>This text should be hidden</span>");
  doc.close();
</script>
