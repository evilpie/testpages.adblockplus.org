template = testcase
title = XMLHTTPRequest Exception
description = Check that usage of the $xmlhttprequest filter option in an exception is working as expected.

<section class="testcase-panel">
  <h2>Exception usage</h2>
  <p>Test that the $xmlhttprequest filter option in an exception works.</p>
  <div class="testcase-area">
    <img src="/testfiles/xmlhttprequest_exception/image.png" data-expectedresult="fail">
    <div id="testcase-status" class="testcase-trigger">Triggering XMLHTTPRequest connection...</div>
    <div class="testcase-expected-view">Text loaded via XMLHTTPRequest Exception</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filters #1 and #2.</li>
    <li>Refresh page.</li>
    <li>The red image should be blocked and the xmlhttprequest should not be blocked, loading a text in the element above.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>||{{ site_url|strip_proto }}/testfiles/xmlhttprequest_exception/*</pre></li>
    <li><pre>@@{{ site_url|strip_proto }}/testfiles/xmlhttprequest_exception/$xmlhttprequest</pre></li>
  </ul>
</section>

<script>
  "use strict";

  let req = new XMLHttpRequest();

  req.onerror = function(e)
  {
    let result = document.getElementById("testcase-status");
    result.innerHTML = "Failed. Connection was blocked.";
    result.setAttribute("data-expectedresult", "fail");
  };
  req.onload = function(e)
  {
    if (this.status == 200)
    {
      let result = document.getElementById("testcase-status");
      result.innerHTML = this.responseText;
      result.setAttribute("data-expectedresult", "pass");
    }
  };

  setTimeout(() =>
  {
    req.open("GET", "{{ site_url }}/testfiles/xmlhttprequest_exception/text.txt", true);
    req.send();
  }, 500);
</script>
