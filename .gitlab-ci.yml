# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2006-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

default:
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind
  before_script:
    - docker info

stages:
  - test

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

.test:
  stage: test
  script:
    - docker build -t testpages --build-arg REVISION=$REVISION .
    - docker run -e BROWSER="$BROWSER" testpages
  after_script:
    - docker cp $(docker ps -aqf ancestor=testpages):/adblockpluschrome/test/screenshots . 2> /dev/null
    - docker cp $(docker ps -aqf ancestor=testpages):/var/log/nginx/ .
  artifacts:
    paths:
      - screenshots/
      - nginx/
    when: on_failure
    expire_in: 1 mo
  interruptible: true

.test_edge:
  stage: test
  tags:
    - shared-windows
    - windows
    - windows-1809
  needs:
    - &build
      job: build:chrome
      artifacts: true
      project: eyeo/adblockplus/adblockpluschrome
  before_script:
    # Install packages
    - choco install -y nginx --version=1.19.0
    - $Env:NGINX = "C:\tools\nginx-1.19.0"; $Env:NGINX_LINUX = "C:/tools/nginx-1.19.0"
    - choco install -y openssl.light
    - $Env:Path += ";C:\Program Files\OpenSSL\bin\"
    - choco install -y microsoft-edge --version=79.0.309.71
    - choco install -y python2
    - $Env:Path += ";C:\Python27;C:\Python27\Scripts"
    # nginx config
    - xcopy test\etc\nginx\* $Env:NGINX\conf\ /E /V /F /H
    - (Get-Content $Env:NGINX\conf\nginx.conf).replace(
      "octet-stream;", "octet-stream;`n include $Env:NGINX_LINUX/conf/sites-enabled-windows/*.conf;")
      | Set-Content $Env:NGINX\conf\nginx.conf
    - openssl req -x509 -newkey rsa:4096
      -keyout $Env:NGINX\conf\local.testpages.adblockplus.org_key.pem
      -out $Env:NGINX\conf\local.testpages.adblockplus.org_cert.pem
      -days 365 -nodes -subj "/CN=local.testpages.adblockplus.org"
    - echo "`n127.0.0.1 local.testpages.adblockplus.org" | Add-Content c:\windows\system32\drivers\etc\hosts
    # Build CMS
    - git clone https://gitlab.com/eyeo/websites/cms.git
    - pip install -r cms\requirements.txt
    # Build adblockpluschrome
    - git clone https://gitlab.com/eyeo/adblockplus/adblockpluschrome.git
    - Expand-Archive -Path adblockpluschrome-*.zip -DestinationPath adblockpluschrome\devenv.chrome
    - cd adblockpluschrome; git checkout $Env:REVISION; npm install; cd ..
    # Generate test pages files
    - mkdir -p $Env:NGINX\html\local.testpages.adblockplus.org
    - (Get-Content settings.ini).replace("testpages.adblockplus.org", "local.testpages.adblockplus.org")
      | Set-Content settings.ini
    - $Env:PYTHONPATH = "cms"
    - python -m cms.bin.generate_static_pages . $Env:NGINX\html\local.testpages.adblockplus.org
  script:
    # Run testpages server
    - Start-Process -NoNewWindow "$Env:NGINX\nginx.exe" -WorkingDirectory "$Env:NGINX" -PassThru
    # Run tests
    - cd adblockpluschrome
    - echo "INFO - Tests will execute based on the following revision:";
      git status | Select -First 1; git log -5 --oneline
    # Sitekey test disabled - testpages#41
    - $npm_process = (Start-Process -NoNewWindow "npm"
      -ArgumentList "run test-only -- -g '`"^`"Edge((?!qunit`"|`"Sitekey`"|`"Final).)*$'" -PassThru -Wait)
    - Stop-Process -Name "nginx" -Force -PassThru
    - Start-Sleep -Seconds 3 # Allow npm process to finish
    - Exit($npm_process.ExitCode)
  variables:
    TEST_PAGES_URL: https://local.testpages.adblockplus.org/en/
    TEST_PAGES_INSECURE: "true"
    SKIP_BUILD: "true"
  artifacts:
    paths:
      - adblockpluschrome/test/screenshots/
    when: on_failure
    expire_in: 1 mo
  interruptible: true

.master:
  extends: .test
  variables:
    REVISION: master

.next:
  extends: .test
  variables:
    REVISION: next

lint:
  stage: test
  needs: []
  script:
    - docker build -t lintimage -f test/lint.Dockerfile .
    - docker run lintimage
  interruptible: true

master:chrome:latest:
  extends: .master
  variables:
    BROWSER: Chromium \(latest\)

master:chrome:oldest:
  extends: .master
  variables:
    BROWSER: Chromium \(oldest\)

master:edge:
  extends: .test_edge
  needs:
    - <<: *build
      ref: master
  variables:
    REVISION: master

master:firefox:latest:
  extends: .master
  variables:
    BROWSER: Firefox \(latest\)

master:firefox:oldest:
  extends: .master
  variables:
    BROWSER: Firefox \(oldest\)

next:chrome:latest:
  extends: .next
  variables:
    BROWSER: Chromium \(latest\)

next:chrome:oldest:
  extends: .next
  variables:
    BROWSER: Chromium \(oldest\)

next:edge:
  extends: .test_edge
  needs:
    - <<: *build
      ref: next
  variables:
    REVISION: next

next:firefox:latest:
  extends: .next
  variables:
    BROWSER: Firefox \(latest\)

next:firefox:oldest:
  extends: .next
  variables:
    BROWSER: Firefox \(oldest\)
